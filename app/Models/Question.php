<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_userqa',
        'question_content',
        'is_answered',
        'is_read',
        'answer_content',
        'admin_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_userqa');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id');
    }
}
