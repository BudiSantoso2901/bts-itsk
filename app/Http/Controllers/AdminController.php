<?php

namespace App\Http\Controllers;

use App\Models\FilePendukung;
use App\Models\kegiatan;
use App\Models\Question;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    public function dashboard()
    {
        // Hitung jumlah kegiatan yang sedang diproses dan diterima
        $jumlahDiproses = Kegiatan::where('status_promosi', 'Diproses')->count();
        $jumlahDiterima = Kegiatan::where('status_promosi', 'Diterima')->count();
        $unansweredCount = Question::where('is_answered', false)->count();
        
        return view('admin.dashboard.admin.admin_index', [
            'jumlahDiproses' => $jumlahDiproses,
            'jumlahDiterima' => $jumlahDiterima,
            'unansweredCount' => $unansweredCount
        ]);
    }
    public function index_proses()
    {
        $kegiatansDiproses = Kegiatan::where('status_promosi', 'Diproses')
            ->with(['users' => function ($query) {
                $query->wherePivot('jabatan', 'Ketua')->orWherePivot('jabatan', 'Dosen');
            }])
            ->get();
        // Hitung jumlah kegiatan yang sedang diproses dan diterima
        $jumlahDiproses = Kegiatan::where('status_promosi', 'Diproses')->count();
        $jumlahDiterima = Kegiatan::where('status_promosi', 'Diterima')->count();

        return view('admin.dashboard.admin.admin_pendaftar', [
            'kegiatans' => $kegiatansDiproses,
            'jumlahDiproses' => $jumlahDiproses,
            'jumlahDiterima' => $jumlahDiterima
        ]);
    }

    public function index_acc()
    {
        $Noacc = Kegiatan::whereIn('status_promosi', ['Ditolak', 'Diterima'])
            ->with(['users' => function ($query) {
                $query->wherePivot('jabatan', 'Ketua')->orWherePivot('jabatan', 'Dosen');
            }])
            ->get();

        // Hitung jumlah kegiatan yang sedang diproses dan diterima
        $jumlahDiproses = Kegiatan::where('status_promosi', 'Diproses')->count();
        $jumlahDiterima = Kegiatan::where('status_promosi', 'Diterima')->count();

        return view('admin.dashboard.admin.admin_telah_selesai', [
            'kegiatans' => $Noacc,
            'jumlahDiproses' => $jumlahDiproses,
            'jumlahDiterima' => $jumlahDiterima
        ]);
    }

    // FILE PENDUKUNG BAHAN PRESENTASI 
    public function viewFilePendukung(Request $request)
    {
        $search = $request->input('search');

        $file_pendukung = FilePendukung::when($search, function ($query, $search) {
            $query->where('nama', 'like', '%' . $search . '%')
                ->orWhere('jenis', 'like', '%' . $search . '%');
        })
            ->get();

        return view('admin.file_pendukung.view', compact('file_pendukung', 'search'));
    }
    public function create_file_pendukung()
    {
        return view('admin.file_pendukung.create');
    }
    public function store_file(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jenis' => 'required',
            'file' => 'required|file',
        ]);

        $file = $request->file('file');
        $nama_file = Str::random(10) . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('public/file_presentasi', $nama_file);

        $url = asset(Storage::url($path));

        FilePendukung::create([
            'nama' => $request->nama,
            'jenis' => $request->jenis,
            'file' => $nama_file,
            'url' => $url,
            'is_active' => true,
        ]);
        return redirect()->route('Admin.file_pendukung.view')->with('success', 'File pendukung berhasil ditambahkan.');
    }
    public function update_file_status($id)
    {
        $file_pendukung = FilePendukung::findOrFail($id);
        $file_pendukung->is_active = !$file_pendukung->is_active;
        $file_pendukung->save();

        return redirect()->route('Admin.file_pendukung.view')->with('success', 'Status file pendukung berhasil diubah.');
    }

    // Fungsi untuk menghapus file pendukung
    public function delete_file($id)
    {
        $file_pendukung = FilePendukung::findOrFail($id);
        Storage::delete('public/file_presentasi/' . $file_pendukung->file);
        $file_pendukung->delete();

        return redirect()->route('Admin.file_pendukung.view')->with('success', 'File pendukung berhasil dihapus.');
    }

    public function pesanMasuk()
    {
        $questions = Question::where('is_answered', false)->get();

        return view('admin.dashboard.admin.admin_pesan_masuk', compact('questions'));
    }

    public function balasPesan(Question $question)
    {
        return view('admin.dashboard.admin.admin_balas_pesan', compact('question'));
    }
}
