<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pivot_laporan_files', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_lp')->references('id')->on('laporans');
            $table->foreignId('id_fl')->references('id')->on('file_laporans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pivot_laporan_files');
    }
};
