@extends('layouts.main')

@section('title', 'BTS-ITSK | Status')

@section('content')
    {{-- NAVBAR  --}}
    @include('partials.navbar')

    {{-- RIWAYAT PENDAFTARAN  --}}
    @include('partials.riwayatpendaftaran')

    {{-- FOOTER  --}}
    @include('partials.footer')
@endsection
