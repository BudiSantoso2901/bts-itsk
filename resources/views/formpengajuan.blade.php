@extends('layouts.mainform')

@section('title', 'BTS-ITSK | Form Pengajuan')

@section('registration')
    {{-- NAVBAR  --}}
    @include('partials.navbar')

    @include('partials.formpendaftaran')

    {{-- FOOTER  --}}
    @include('partials.footer')
@endsection