@extends('login.loginform')

@section('title', 'BTS-ITSK | Sign In')

@section('container')

    <div class="col-12 col-sm-10 col-xl-8 p-4 p-sm-5">
        <h1 class="fs-2 mb-5">Selamat Datang</h1>
        <form action="{{ route('login.submit') }}" method="post">
            @csrf
            <label for="email" class="form-label fw-semibold mb-0">Email</label>
            <input type="email" class="form-control mb-3 p-2" id="email" placeholder="Masukkan email" name="email"
                required>

            <div class="d-flex justify-content-between align-items-center">
                <label for="password" class="form-label fw-semibold mb-0">Password</label>
                <a href="{{ route('forget.password.get') }}" class="text-primary text-decoration-none fw-semibold"
                    style="font-size: 13px;">Lupa Password?</a>
            </div>
            <input type="password" class="form-control mb-3 p-2" id="password" placeholder="Masukkan password"
                name="password" required>

            <input class="form-check-input" type="checkbox" id="ingat" name="ingat">
            <label for="ingat" class="form-check-label fw-semibold mb-3" style="font-size: 13px; margin-left: 5px;">Ingat
                selama 30
                hari</label>

            <button type="submit" class="btn btn-dark w-100 p-2">Masuk</button>
        </form>
        <p class="text-center fw-semibold mt-3" style="font-size: 13px;">Atau</p>
        <p class="text-center fw-semibold mt-5 mb-0" style="font-size: 13px;">Belum mempunyai akun? <a
                href="{{ url('/signup') }}" class="text-primary text-decoration-none">Daftar</a></p>
    </div>

@endsection
