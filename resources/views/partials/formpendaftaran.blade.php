<section id="formpendaftaran">
    <div class="container mb-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body p-4">
                        <div class="card-header bg-white text-center">
                            <h5>Form Pendaftaran</h5>
                        </div>
                        <form action="{{ route('kegiatan.store') }}" method="POST">
                            @csrf
                            <div class="form-group pt-3">
                                <label for="nama">
                                    <h6>Nama: </h6>
                                </label>
                                <input type="text" class="form-control" name="id_user" id="id_user"
                                    value="{{ Auth::user()->nama }}" placeholder="Masukkan Nama" required disabled>
                                {{-- <input type="text" class="form-control" name="id_user" id="id_user"  value="" placeholder="Masukkan Nama"> --}}
                            </div>
                            <div class="form-group pt-3">
                                <label for="email">
                                    <h6>Email: </h6>
                                </label>
                                <input type="email" class="form-control" name="id_user" id="id_user"
                                    value="{{ Auth::user()->email }}" placeholder="Masukkan Email" required disabled>
                                {{-- <input type="email" class="form-control" name="id_user" id="id_user" value="" placeholder="Masukkan Email"> --}}
                            </div>
                            <div class="form-group pt-3">
                                <label for="tanggal">
                                    <h6>Tanggal Pelaksanaan:</h6>
                                </label>
                                <input type="datetime-local" class="form-control" name="tanggal_kegiatan" id="tanggal"
                                    required>
                            </div>
                            <div class="form-group pt-3">
                                <label for="provinsi">
                                    <h6>Provinsi Sekolah:</h6>
                                </label>
                                <select class="form-control" name="id_provinsi" id="id_provinsi" required>
                                    <option value="">Pilih provinsi asal sekolah anda</option>
                                    @foreach ($prv as $provinsi)
                                        <option value="{{ $provinsi->id }}">{{ $provinsi->provinsi }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group pt-3">
                                <label for="sekolah">
                                    <h6>Asal Sekolah:</h6>
                                </label>
                                <input type="text" class="form-control" name="sekolah" id="sekolah" value=""
                                    placeholder="Masukkan nama sekolah anda">
                            </div>
                            <div class="form-group pt-3">
                                <label for="anggota">
                                    <h6>Anggota:</h6>
                                </label>

                                <select class="form-control" name="id_user[]" id="select_users" required multiple>
                                    {{-- <option value="">Pilih nama anggota anda</option> --}}
                                    @foreach ($usr as $user)
                                        @if ($user->role === 'mahasiswa' && $user->prodi === Auth::user()->prodi && $user->id !== Auth::id())
                                            <option value="{{ $user->id }}">{{ $user->nama }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <button type="submit" class="btn btn-dark d-block mx-auto px-5 mb-4 mt-4">Kirim</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


{{-- <form action="">
        <select name="countries" id="countries" multiple>
            <option value="1">Afghanistan</option>
            <option value="2">Australia</option>
            <option value="3">Germany</option>
            <option value="4">Canada</option>
            <option value="5">Russia</option>
        </select>
    </form> --}}
